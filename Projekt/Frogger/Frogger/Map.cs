﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frogger
{
    class Map
    {
        public Tile[,] tileList = new Tile[16,9];
        public ArrayList entityList;
        public Vector2 tileSize;
        public Texture2D[] textures;
        public Frog frog;
        public Map(int windowWidth, int windowHeight, int level, Texture2D[] textures, Frog frog)
        {
            this.frog = frog;
            this.textures = textures;
            tileSize = new Vector2(windowWidth / 16f, windowHeight / 9f);
            entityList = new ArrayList();
            /*
            for (int i = 0; i < 9; i++)
            {
                generateLine(i, 0);
            }
            */
            generateMap(0);
        }

        public void update(GameTime gt, KeyboardState keyNow, KeyboardState keyOld)
        {
            frog.update(gt, keyNow, keyOld);
            frog.endOfMap();
        }

        public void draw(GameTime gameTime, SpriteBatch sB)
        {
            for (int i = 0; i < 16; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    tileList[i, j].draw(gameTime, sB);
                }
            }
            foreach (Entity e in entityList)
            {
                e.draw(gameTime, sB);
            }
            frog.draw(gameTime, sB);
        }

        public void generateMap(int level)
        {
            Random r = new Random();
            generateLine(0, 0);
            generateLine(8, 0);
            switch (level)
            {
                case 0:
                    generateLine(1, 0);
                    int x = r.Next(1, 3);
                    generateLine(2, x);
                    if (x == 2)
                    {
                        entityList.Add(new Entity(500, tileSize.Y * 2, 0.01f, 2, 7, textures[7]));
                    }
                    generateLine(5, r.Next(1, 3));
                    generateLine(3, 0);
                    generateLine(4, 0);
                    generateLine(6, 0);
                    generateLine(7, 0);
                    break;
                default:
                    break;
            }
        }

        public void generateLine(int row, int type)
        {
            for (int i = 0; i < 16; i++)
            {
                tileList[i, row] = new Tile(tileSize.X * i, tileSize.Y * row, new Vector2(tileSize.X / 256, tileSize.Y / 256), type, textures[type]);
            }
        }
    }
}

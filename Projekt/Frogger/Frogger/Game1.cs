﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Frogger
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Frogger : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public Texture2D grass;
        public Texture2D grassWater;
        public Texture2D waterGrass;
        public Texture2D water;
        public Texture2D waterStone;
        public Texture2D frogGreen;
        public Texture2D frogBlue;
        public Texture2D frogRed;
        public Texture2D[] textureList;

        Map activeMap;
        KeyboardState keyNow;
        KeyboardState keyOld;

        public Frogger()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = 1366;
            graphics.PreferredBackBufferHeight = 768;
            graphics.ApplyChanges();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            this.IsMouseVisible = true;

           

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            textureList = new Texture2D[9];


            textureList[0] = this.Content.Load<Texture2D>("Sprites/Textures/Grass");
            textureList[1] = this.Content.Load<Texture2D>("Sprites/Textures/Street");
            textureList[2] = this.Content.Load<Texture2D>("Sprites/Textures/Water");
            textureList[3] = this.Content.Load<Texture2D>("Sprites/Textures/Grass Water");
            textureList[4] = this.Content.Load<Texture2D>("Sprites/Textures/Water Grass");
            textureList[5] = this.Content.Load<Texture2D>("Sprites/Textures/Water Stone");
            textureList[6] = this.Content.Load<Texture2D>("Sprites/Frogs/Richtig/Green Frog Richtig");
            textureList[7] = this.Content.Load<Texture2D>("Sprites/Frogs/Richtig/Black Red Frog Richtig");
            textureList[8] = this.Content.Load<Texture2D>("Sprites/Frogs/Richtig/Blue Black Frog Richt");
            //frog = new Frog(GraphicsDevice.PresentationParameters.BackBufferWidth, GraphicsDevice.PresentationParameters.BackBufferHeight, textureList[6]);
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            keyNow = Keyboard.GetState();
            if (keyNow.IsKeyDown(Keys.Escape))
                Exit();
            if (activeMap == null)
            {
                activeMap = new Map(GraphicsDevice.PresentationParameters.BackBufferWidth, 
                    GraphicsDevice.PresentationParameters.BackBufferHeight, 
                    0, 
                    textureList,
                    new Frog(GraphicsDevice.PresentationParameters.BackBufferWidth, 
                        GraphicsDevice.PresentationParameters.BackBufferHeight, 
                        textureList[6]
                    )
                );
            }

            activeMap.update(gameTime, keyNow, keyOld);
            
            // TODO: Add your update logic here

            keyOld = keyNow;
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.White);

            // TODO: Add your drawing code here
            spriteBatch.Begin();
            activeMap.draw(gameTime, spriteBatch);
            

            spriteBatch.End();


            base.Draw(gameTime);
        }
    }
}

﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frogger
{
    class Tile
    {
        Vector2 position;
        Vector2 size;
        int type;
        Texture2D texture;

        public Tile(float x, float y, Vector2 scale, int type, Texture2D texture)
        {
            this.position = new Vector2(x, y);
            this.size = scale;
            this.type = type;
            this.texture = texture;
        }
        
        public void draw(GameTime gt, SpriteBatch sB)
        { 
            sB.Draw(texture, position, null, Color.White, 0f, Vector2.Zero, size, SpriteEffects.None, 0f);
        }

    }
}

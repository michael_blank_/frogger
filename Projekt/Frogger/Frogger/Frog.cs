﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frogger
{
    class Frog
    {
        int x;
        int y;
        int gameWidth;
        int gameHeight;
        Texture2D texture;
        Vector2 scale;

        public Frog(int width, int height, Texture2D texture)
        {
            this.x = (width / 2) - ((width / 16) / 2);
            this.y = (height / 9) + 7 * (height / 9) ;
            this.gameHeight = height;
            this.gameWidth = width;
            Vector2 tileSize = new Vector2(width / 16f, height / 9f);
            scale = new Vector2(tileSize.X / 256, tileSize.Y / 256);
            this.texture = texture;
        }

        public void moveLeft()
        {
            if (this.x - (gameWidth/16) <= 0)
            {
                this.x = 0;
            }
            else
            {
                this.x = this.x - (gameWidth / 16);
            }
        }

        public void moveRight()
        {
            if ((this.x + (gameWidth / 16)) + (gameWidth / 16) >= gameWidth)
            {
                this.x = gameWidth - (gameWidth / 16);
            }
            else
            {
                this.x = this.x + (gameWidth / 16);
            }
        }
        
        public void moveUp()
        {
            if (this.y == 0) { this.y = this.y; }
            else { this.y = this.y - gameHeight / 9; }
        }

        public void moveDown()
        {
            if ( this.y >= (8 * (gameHeight/9)))
            {
                this.y = this.y;
            }
            else
            {
                this.y = this.y + gameHeight/9;
            }
        }

        public Vector2 update(GameTime gt, KeyboardState newKey, KeyboardState oldKey)
        {
            System.Console.Write("test");
            if (newKey.IsKeyDown(Keys.Up) && oldKey.IsKeyUp(Keys.Up))
            {
                System.Console.Write("lol");
                moveUp(); 
            }

            if (newKey.IsKeyDown(Keys.Down) && oldKey.IsKeyUp(Keys.Down))
            {
                moveDown();
            }

            if (newKey.IsKeyDown(Keys.Right) && oldKey.IsKeyUp(Keys.Right))
            {
                moveRight();
            }

            if (newKey.IsKeyDown(Keys.Left) && oldKey.IsKeyUp(Keys.Left))
            {
                moveLeft();
            }
            Vector2 position = new Vector2(this.x, this.y);

            return position;
        }
        public void draw(GameTime gameTime, SpriteBatch sB)
        {
            System.Console.WriteLine("Test");
            sB.Draw(texture, new Vector2(this.x, this.y), null, Color.White, 0f, Vector2.Zero, scale, SpriteEffects.None, 0f);
        }

        public bool endOfMap()
        {
            if (this.y < gameHeight / 9)
            {
                return true;
            }
            else { return false; }
        }
    }


}

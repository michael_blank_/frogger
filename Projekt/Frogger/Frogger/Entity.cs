﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frogger
{
    class Entity
    {
        Vector2 position;
        float v;
        Vector2 size;
        int type;
        Texture2D texture;

        public Entity(float x, float y, float v, int width, int type, Texture2D texture)
        {
            this.size = new Vector2(x, y);
            this.v = v;
            this.position = new Vector2(x, y);
            this.texture = texture;
        }

        public void update(int width)
        {
            position.X = position.X + v;
            if(position.X >= width && v > 0)
            {
                position.X = 0 - size.X;
            }

            if(position.X == 0 - size.X && v < 0) { position.X = width + size.X; }
        }

        public void draw(GameTime gt, SpriteBatch sB)
        { 
            sB.Draw(texture, position, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
        }

       
    }
}
